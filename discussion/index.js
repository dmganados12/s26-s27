// keywords (in package.json) => identifies the certain kewords that describes 

// GOAL: Create a server using Express.js 
//1. USe the require() directive to acquire the express library and its utilities.
	const express = require('express');

//2. Create a server using only express.
	const server = express();
	// express() => will allow us to create an express application.

// 3. Identify a post/address that will serve/host this newly created conncection/app.
	const address = 3000;

// 4. Bind the appliation to the desired designated port using the listen(). Create a method that will display a response that the connection has been established.
	server.listen(address, () => {
		console.log(`Sever is running on port: ${address}`);
	});

